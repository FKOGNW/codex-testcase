import requests
import json

""" To get data from all kabupaten and kota in Indonesia, I use https://dev.farizdotid.com API to get all kabupaten and kota in Indonesia.
After I get the data, insert the data to a list and then get the detail of current weather from api weather """

def getKabupatenKota():
    # get provinsi data
    res_provinsi = requests.get('https://dev.farizdotid.com/api/daerahindonesia/provinsi').json()['provinsi']
    # get id of provinsi
    list_of_id_provinsi = [provinsi['id'] for provinsi in res_provinsi]


    list_of_kabupaten = [] # empty list of kabupaten
    # looping to get nama of provinsi
    for id_provinsi in list_of_id_provinsi:
        # get kabupaten data
        res_kabupaten_kota = requests.get(f'https://dev.farizdotid.com/api/daerahindonesia/kota?id_provinsi={id_provinsi}').json()['kota_kabupaten']
        # get name of kabupaten
        temp_kabupaten = [kabupaten_kota['nama'] for kabupaten_kota in res_kabupaten_kota]
        list_of_kabupaten.extend(temp_kabupaten) # extend list_of_kabupaten

    # remove string "Kabupaten" and "Kota" from list
    list_of_kabupaten = [s.replace('Kabupaten ', '') for s in list_of_kabupaten]
    list_of_kabupaten = [s.replace('Kota ', '') for s in list_of_kabupaten]

    return list_of_kabupaten


def getWeather(param):
    res_weather = requests.get(f'http://api.weatherapi.com/v1/current.json?key=acf607e5a0864fd1afa62553222712&q={param}&aqi=no')
    if res_weather.status_code != 200:
        return
    
    return res_weather.json()


def main():
    kabupaten_kota = getKabupatenKota()
    
    weather = []
    for kabupaten in kabupaten_kota:
        print(kabupaten)
        res_weather = getWeather(kabupaten)

        if res_weather == None:
            continue

        weather.append(res_weather)

    with open("weather.json", "w") as outfile:
        json.dump(weather, outfile)

if __name__ == "__main__":
    main()