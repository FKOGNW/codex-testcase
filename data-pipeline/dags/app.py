from __future__ import annotations
import json
import pendulum

from airflow import DAG
from airflow.providers.mongo.hooks.mongo import MongoHook
from airflow.hooks.mysql_hook import MySqlHook
from airflow.operators.python import PythonOperator

def dict_flatten(in_dict):
    dict_out = {}
    parent_key = None
    separator = ""

    for k, v in in_dict.items():
        k = f"{parent_key}{separator}{k}" if parent_key else k
        if isinstance(v, dict):
            dict_flatten(in_dict=v, dict_out=dict_out, parent_key=k)
            continue

        dict_out[k] = v

    return dict_out

with DAG(
    "dag-etl-mongo-mysql",
    default_args={"retries": 2},
    schedule_interval="0 1 * * *",
    start_date=pendulum.datetime(2022, 12, 27, tz="UTC"),
    catchup=False,
    tags=["codex"]
    ) as dag:

    def extract(**kwargs):
        # connect to mongo
        hook = MongoHook(mongo_conn_id='mongo_default')
        client = hook.get_conn()
        db = client.local
        res = db.data
        cur = res.find()
        
        data = []
        for cur in x:    
            data.append(cur)

        ti = kwargs["ti"]

        ti.xcom_push("data", data)


    def transform(**kwargs):
        ti = kwargs["ti"]
        extract_data = ti.xcom_pull(
            task_ids="extract", key="data")

        final_data = []
        for row in extract_data:
            temp_final_data = dict_flatten(row)
            final_data.append(temp_final_data)

        final_data = json.dumps(final_data)
        ti.xcom_push("final_data", final_data)

    def load(**kwargs):
        ti = kwargs["ti"]
        data = ti.xcom_pull(
            task_ids="transform", key="final_data")
        res_data = json.loads(data)

        sql = "INSERT INTO data (_id, vaccinePatientprofession, vaccinePatientfullName,\
                vaccinePatientgender, vaccinePatientbornDate, vaccinePatientmobileNumber,\
                vaccinePatientnik, channel, createdAt, updatedAt,\
                vaccinationvaccineLocationname, vaccinationvaccineLocationfaskesCode,\
                vaccinationvaccineCode, vaccinationvaccineDate, vaccinationvaccineStatus,\
                vaccinationtype) VALUES (%(_id)s, %(vaccinePatientprofession)s, %(vaccinePatientfullName)s,\
                %(vaccinePatientgender)s, %(vaccinePatientbornDate)s, %(vaccinePatientmobileNumber)s,\
                %(vaccinePatientnik)s, %(channel)s, %(createdAt)s, %(updatedAt)s,\
                %(vaccinationvaccineLocationname)s, %(vaccinationvaccineLocationfaskesCode)s,\
                %(vaccinationvaccineCode)s, %(vaccinationvaccineDate)s, %(vaccinationvaccineStatus)s,\
                %(vaccinationtype)s))"
        mysql_hook = MySqlHook(mysql_conn_id = 'mysql_default', schema = 'local')
        connection = mysql_hook.get_conn()
        cursor = connection.cursor()
        cursor.executemany(sql, res_data)

    extract_task = PythonOperator(
        task_id="extract",
        python_callable=extract,
    )

    transform_task = PythonOperator(
        task_id="transform",
        python_callable=transform,
    )

    load_task = PythonOperator(
        task_id="load",
        python_callable=load,
    )

    extract_task >> transform_task >> load_task
